package com.example.pbd2022_lab_3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.example.pbd2022_lab_3.RecyclerAdapter.CardViewHolder


class   RecyclerAdapter : RecyclerView.Adapter<CardViewHolder?>() {
    private val titles = arrayOf("Mercury",
            "Venus",
            "Earth",
            "Mars",
            "Jupiter",
            "Saturn",
            "Uranus",
            "Neptune")
    private val details = arrayOf("The smallest planet",
            "The second brightest object in the night sky", "The only known to harbour life",
            "Named after the Roman god of war", "The largest planet in the Solar system",
            "Famous for its rings", "The coldest planet",
            "The farthest from the Sun")
    private val images = intArrayOf(R.drawable.mercury,
            R.drawable.venus,
            R.drawable.earth,
            R.drawable.mars,
            R.drawable.jupiter,
            R.drawable.saturn,
            R.drawable.uranus,
            R.drawable.neptune)

    inner class CardViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var itemImage: ImageView? = itemView?.findViewById(R.id.item_image)
        var itemTitle: TextView? = itemView?.findViewById(R.id.item_title)
        var itemDetail: TextView? = itemView?.findViewById(R.id.item_detail)

       init {
            itemView?.setOnClickListener{view ->
                Snackbar.make(view, adapterPosition.toString() , Snackbar.LENGTH_LONG).show()
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CardViewHolder {
        val bind = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_layout,
            viewGroup, false)
        return (CardViewHolder(bind))
    }

    override fun onBindViewHolder(viewHolder: CardViewHolder, i: Int) {
        viewHolder.itemImage?.setImageResource(images[i])
        viewHolder.itemDetail?.text = details[i]
        viewHolder.itemTitle?.text = titles[i]

    }

    override fun getItemCount(): Int {
        return titles.size
    }
}