package com.example.pbd2022_lab_3

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.example.pbd2022_lab_3.TabPagerAdapter
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
        private const val NUM_OF_TABS = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        configureTabLayout()
    }

    private fun configureTabLayout() {
        val tabLay = findViewById<TabLayout>(R.id.tab_layout)
        val pagerView = findViewById<ViewPager2>(R.id.view_pager_main)
        val novAdapter = TabPagerAdapter(this, NUM_OF_TABS)
        pagerView.adapter = novAdapter
        TabLayoutMediator(tabLay, pagerView)
        { tab, position ->
            when(position) {
                0 -> tab.text = getString(R.string.tab1_title)
                1 -> tab.text = getString(R.string.tab2_title)
                2 -> tab.text = getString(R.string.tab3_title)
            }
        }.attach()
    }


}