package com.example.pbd2022_lab_3

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class TabPagerAdapter(fa: FragmentActivity?, private val tabCounter: Int) :
    FragmentStateAdapter(fa!!) {
    override fun getItemCount(): Int {
        return tabCounter
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> Tab1Fragment()
            1 -> Tab2Fragment()
            2 -> Tab3Fragment()
            else -> {
                Tab1Fragment()
            }
        }
    }
}
